README

1) Neues Git-Repo anlegen
	NAME: cloud-computing-paas
	LINK: https://gitlab.com/katrinpetschnig/cloud-computing-paas.git

2) Git-Repo lokal auschecken und mit Readme, package.js, server.js und initdb_postgres.sql inizialisieren
	(aus Example-Beispiel)

3) initdb_postgres.sql umändern auf die neuen Tabellen

   SNIPPETS
	-> id (int)
	-> name (string)
	-> description (string)
	-> author (string)
	-> language (string)
	-> code (string)

   TAGS
	-> id (int)
	-> name (string)
	-> snippetId (int)

4) package.json umändern auf neuen Namen

5) server.js umändern und neue REQUESTS einbauen

	GET snippets
	GET snippets/:id
	POST snippets
	PUT snippets/:id
	DELTETE snippets/:id

6) PATH=$PATH:$HOME/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit

7) oc cluster up

The server is accessible via web console at:
    https://127.0.0.1:8443

You are logged in as:
    User:     developer
    Password: <any value>

To login as administrator:
    oc login -u system:admin

8)  oc new-app --template=postgresql-persistent -p POSTGRESQL_USER=snippets -p POSTGRESQL_PASSWORD=keines -p POSTGRESQL_DATABASE=snippets --name=snippetsdb

            Username: snippets
            Password: keines
       Database Name: snippets
      Connection URL: postgresql://postgresql:5432/
     
9) oc get svc -l app=snippetsdb
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
postgresql   ClusterIP   172.30.151.90   <none>        5432/TCP   4s

10) cd cloud-computing-paas

11) psql -h 172.30.151.90 -U snippets -W snippets <initdb_postgres.sql
DROP TABLE
DROP TABLE
CREATE TABLE
CREATE TABLE
INSERT 0 3
INSERT 0 6

12) CHECKING DATABASE
psql -h 172.30.151.90 -U snippets -W snippets
>\dt
           Liste der Relationen
 Schema |   Name   |   Typ   | Eigentümer 
--------+----------+---------+------------
 public | snippets | Tabelle | snippets
 public | tags     | Tabelle | snippets


> select * from snippets;
 id |    name     |     description      | author |  language  |                  code                  
----+-------------+----------------------+--------+------------+----------------------------------------
  1 | Hello World | print a String value | john   | Java       | System.println("Hello World");
  2 | Loop        | typical For-Loop     | katrin | Javascript | for (let i = 0; i < arr.length; i++){}
  3 | Increment   | Inkrements var by 1  | katrin | C          | var++
(3 Zeilen)

> select * from tags;
 id |   name    | snippetid 
----+-----------+-----------
  1 | simple    |         1
  2 | print     |         1
  3 | beginner  |         1
  4 | loop      |         2
  5 | for       |         2
  6 | increment |         3
(6 Zeilen)

13) oc new-app . --name snippets -e DATABASE_URL="postgres://snippets:keines@postgresql.myproject.svc.cluster.local:5432/snippets"

--> Creating resources ...
    imagestream.image.openshift.io "snippets" created
    buildconfig.build.openshift.io "snippets" created
    deploymentconfig.apps.openshift.io "snippets" created
    service "snippets" created
--> Success
    Build scheduled, use 'oc logs -f bc/snippets' to track its progress.
    Application is not exposed. You can expose services to the outside world by executing one or more of the commands below:
     'oc expose svc/snippets' 

> oc logs -f bc/snippets
Push successful

14) oc get pods

NAME                 READY     STATUS             RESTARTS   AGE
postgresql-1-ppscg   1/1       Running            0          20m
snippets-1-9sqh6     0/1       CrashLoopBackOff   2          38s
snippets-1-build     0/1       Completed          0          3m


15) Get Cluster-IP
oc get svc snippets
NAME       TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
snippets   ClusterIP   172.30.129.189   <none>        8080/TCP   20s


16) oc logs -f service/snippets
Connecting to database  ...
SnippetsAPI listening at http://:::8080/snippets
Connection to database established!

17) curl http://172.30.129.189:8080/snippets/1
{"id":1,"name":"Hello World","description":"print a String value","author":"john","language":"Java","code":"System.println(\"Hello World\");","tags":["simple","print","beginner“]}

18)  oc expose service/snippets
route.route.openshift.io/snippets exposed	

19) oc get route
NAME       HOST/PORT                             PATH      SERVICES   PORT       TERMINATION   WILDCARD
snippets   snippets-myproject.127.0.0.1.nip.io             snippets   8080-tcp                 None

20) curl snippets-myproject.127.0.0.1.nip.io:snippets/1
{"id":1,"name":"Hello World","description":"print a String value","author":"john","language":"Java","code":"System.println(\"Hello World\");","tags":["simple","print","beginner“]}

HEROKU
1) npm link pg express body-parser

2) Bei Heroku einloggen
heroku login -i

3) heroku apps:create --addons=heroku-postgresql:hobby-dev
salty-taiga-81483

4) Datenbank initialisieren
heroku pg:psql <initdb_postgres.sql -a salty-taiga-81483

NOTICE:  table "tags" does not exist, skipping
DROP TABLE
NOTICE:  table "snippets" does not exist, skipping
DROP TABLE
CREATE TABLE
CREATE TABLE
INSERT 0 3
INSERT 0 6

5)export DATABASE_URL=$(heroku config:get -a salty-taiga-81483 DATABASE_URL)
export SSL=true

6) heroku git:remote -a salty-taiga-81483

7) git push heroku master

8) open https://salty-taiga-81483.herokuapp.com/snippets/1 in browser


