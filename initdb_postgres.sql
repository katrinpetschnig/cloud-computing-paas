DROP TABLE IF EXISTS tags;
DROP TABLE IF EXISTS snippets;

CREATE TABLE snippets (
  id SERIAL PRIMARY KEY,
  name varchar(30) DEFAULT NULL,
  description varchar(100) DEFAULT NULL,
  author varchar(40) DEFAULT NULL,
  language varchar(30) DEFAULT NULL,
  code varchar(255) DEFAULT NULL
);

CREATE TABLE tags (
  id SERIAL PRIMARY KEY,
  name varchar(30) DEFAULT NULL,
  snippetId integer REFERENCES snippets(id)
);

INSERT INTO snippets (name, description, author, language, code) VALUES
  ('Hello World', 'print a String value', 'john', 'Java',
   'System.println("Hello World");'), 
  ('Loop', 'typical For-Loop', 'katrin', 'Javascript', 
   'for (let i = 0; i < arr.length; i++){}'),  
  ('Increment', 'Inkrements var by 1', 'katrin', 'C', 'var++');

INSERT INTO tags (name, snippetId) VALUES
  ('simple', (SELECT id FROM snippets WHERE name = 'Hello World')),
  ('print', (SELECT id FROM snippets WHERE name = 'Hello World')),
  ('beginner', (SELECT id FROM snippets WHERE name = 'Hello World')),
  ('loop', (SELECT id FROM snippets WHERE name = 'Loop')),
  ('for', (SELECT id FROM snippets WHERE name = 'Loop')),
  ('increment', (SELECT id FROM snippets WHERE name = 'Increment'));