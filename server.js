"use strict";

/**
 * Cloud Computing Example Service
 * 
 * Intended to be deployed on OKD/OpenShift or Heroku PaaS platforms
 */

// Get default settings from environment variables
const SSL = process.env.SSL || "false";
const SERVER_PORT = process.env.EXAMPLE_SERVICE_PORT || process.env.PORT || 8080; // PORT environement variable provided by Heroku
const SERVER_PREFIX = process.env.EXAMPLE_SERVICE_PREFIX || "/snippets";
const DB_URL = process.env.EXAMPLE_DB_URL || process.env.DATABASE_URL || "postgres://snippets:keines@127.0.0.1:5432/snippets";

/** Postgres database access functions objects */
class PSQL_DB {

    /** Create a database connection
     * @constructs PSQL_DB, a PostgreSQL database connection
     * @param {string} url - complete database connection url
    */
    constructor(url) {
        const { Client } = require('pg');
    	console.log(`Using Database URL: ${url}`);
    	var use_ssl = (SSL == "true" || SSL == 1 ? true : false);
        this.connection = new Client({ 
            connectionString: url, 
            ssl: use_ssl 
        });

        // connect to the database
        this.connect();

        // if connection to DB has been closed unexpected
        this.connection.on('end', (error) => {
            console.log('Connection closed ', error);
            // try to re-connect
            this.connect();
        });
    }

    /** Connect to the database */
    connect() {
        console.log(`Connecting to database  ...`);
        this.connection.connect((error) => {
            if (error) {
                console.log(`Connection to database FAILED!`);
		console.log(error);
                process.exit(1);
            }
            else {
                console.log(`Connection to database established!`);
            }
        });
    }

    /** Get snippet with given id
     * @param {id} id - id of snippet
     * @returns {Promise} - Promise for the snippet query
     */
    dbGetSnippet(id) {
        return this.connection.query('SELECT id, name, description, author, language, code FROM snippets WHERE id = $1', [id]);
    }

    /** Get tags of snippet with id
     * @param {id} id - id of snippet
     * @returns {Promise} - Promise for the tags query
     */
    dbGetTagsOfSnippet(id) {
        return this.connection.query('SELECT id, name, snippetId FROM tags WHERE snippetId = $1', [id]);
    }

    /** Get all snippets
     * @returns {Promise} - Promise for the snippet query
     */
    dbGetSnippets() {
        return this.connection.query('SELECT id, name, description, author, language, code FROM snippets');
    }

    /** Deletes snippet with id
     * @param {id} id - id of snippet
     * @returns {Promise} - Promise for the snippet query
     */
    dbDeleteSnippet(id) {
        return this.connection.query('DELETE FROM snippets WHERE id = $1', [id]);
    }

    /** Deletes tags of snippet
     * @param {id} id - id of snippet
     * @returns {Promise} - Promise for the tags query
     */
    dbDeleteTagsOfSnippet(id) {
        return this.connection.query('DELETE FROM tags WHERE snippetId = $1', [id]);
    }

    /** creates snippet
     * @param {name} name - name of snippet
     * @param {description} description - description of snippet
     * @param {author} author - author of snippet
     * @param {language} language - language of snippet
     * @param {code} code - code of snippet
     * @returns {Promise} - Promise for the snippets query
     */
    dbCreateSnippet(name, description, author, language, code) {
        return this.connection.query('INSERT INTO snippets VALUES($1, $2, $3, $4, $5)', [name, description, author, language, code]);
    }

    /** creates tag
     * @param {name} name - name of tag
     * @param {snippetId} snippetId - id of snippet
     * @returns {Promise} - Promise for the tags query
     */
    dbCreateTag(name, snippetId) {
        return this.connection.query('INSERT INTO tags VALUES($1, $2)', [name, snippetId]);
    }

    /** creates snippet
     * @param {id} id - id of snippet
     * @param {name} name - name of snippet
     * @param {description} description - description of snippet
     * @param {author} author - author of snippet
     * @param {language} language - language of snippet
     * @param {code} code - code of snippet
     * @returns {Promise} - Promise for the snippets query
     */
    dbUpdateSnippet(id, name, description, author, language, code) {
        return this.connection.query('UPDATE snippets SET name = $1, description = $2, author = $3, language = $4, code = $5 WHERE id = $6', [name, description, author, language, code, id]);
    }

    /** queries snippet
     * @param {name} name - name of snippet
     * @param {description} description - description of snippet
     * @param {author} author - author of snippet
     * @param {language} language - language of snippet
     * @param {code} code - code of snippet
     * @returns {Promise} - Promise for the snippets query
     */
    dbQuerySnippet(name, description, author, language, code) {
        let started = false;
        let query = "SELECT id FROM snippets ";
        let params = [];
        let counter = 1;
        console.log("name = " + name);
        if (name != "") {
            query += " WHERE name = $" + counter + " ";
            started = true;
            counter++;
            params.push(name);
        }
        console.log("description = " + description);
        if (description != "") {
            if (!started) {
                query += " WHERE description = $" + counter;
            } else {
                query += " AND description = $" + counter;
            }
            started = true;
            counter++;
            params.push(description);
        }
        console.log("author = " + author);
        if (author != "") {
            if (!started) {
                query += " WHERE author = $" + counter;
            } else {
                query += " AND author = $" + counter;
            }
            started = true;
            counter++;
            params.push(author);
        }
        console.log("language = " + language);
        if (language != "") {
            if (!started) {
                query += " WHERE language = $" + counter;
            } else {
                query += " AND language = $" + counter;
            }
            started = true;
            counter++;
            params.push(language);
        }
        console.log("code = " + code);
        if (code != "") {
            if (!started) {
                query += " WHERE code = $" + counter;
            } else {
                query += " AND code = $" + counter;
            }
            started = true;
            counter++;
            params.push(code);
        }

        return this.connection.query(query, params);
    }

    /** creates tag
     * @param {tag} tag - name of tag
     * @returns {Promise} - Promise for the tags query
     */
    dbQueryTags(tag) {
        return this.connection.query('SELECT snippetId FROM tags WHERE name = $1', [tag]);
    }
}

/** Class implementing the ReST Example API */
class SnippetsAPI {

    /** Get the message specified by the id paramaeter
     * @param {Object} req - HTTP request as provided by express
     * @param {Object} res - HTTP request as provided by express
     */
    async getById(req, res) {
        var resultSnippet = null;
        var resultTags = null;

        try {
            resultSnippet = await db.dbGetSnippet(req.params.id);
            resultTags = await db.dbGetTagsOfSnippet(req.params.id);
            if (resultSnippet.rows[0] == undefined)  {
                res.json({ "error": "snippet id not found" });
            } else {
                let snippet = resultSnippet.rows[0];
                let tags = [];
                resultTags.rows.forEach(row => {
                    tags.push(row.name);
                });
                snippet.tags = tags;
                res.json(snippet);
            }
        } catch (error) {
            console.log(JSON.stringify(error));
            res.status(500).json({ "error": "database access error" });
        }
    }

    async getAll(req, res) {
        var resultSnippets = null;

        try {
            resultSnippets = await db.dbGetSnippets();
            for (let i = 0; i < resultSnippets.rows.length; i++) {
                let snippet = resultSnippets.rows[i];
                let resultTags = await db.dbGetTagsOfSnippet(snippet.id);
                let tags = [];
                resultTags.rows.forEach(row => {
                    tags.push(row.name);
                });
                snippet.tags = tags;
            }
            res.json(resultSnippets.rows);
        } catch (error) {
            console.log(JSON.stringify(error));
            res.status(500).json({ "error": "database access error" });
        }
    }

    async deleteById(req, res) {
        let rows = null;
        try {
            let deletedTags = await db.dbDeleteTagsOfSnippet(req.params.id);
            rows = await db.dbDeleteSnippet(req.params.id);

            if (rows == undefined || rows == 0)  {
                res.json({ "error": "snippet id not found" });
            } else {
                res.json({ "success": "snippet with id= " + req.params.id + " got deleted and " + deletedTags + " tags." });
            }
        } catch (error) {
            console.log(JSON.stringify(error));
            res.status(500).json({ "error": "database access error" });
        }
    }

    async createSnippet(req, res) {
        let result = null;
        try {
            let body = req.body;
            result = await db.dbCreateSnippet(body.name, body.description, body.author, body.lang, body.code);
            if (result == undefined || result.rows.length == 0)  {
                res.json({ "error": "snippet could not be created" });
            }

            for (let i = 0; i < body.tags.length; i++) {
                let tag = body.tags[i];
                await db.dbCreateTag(tag, result.rows[0].id);
            }
            res.json({ "id": result.rows[0] });
        } catch (error) {
            console.log(JSON.stringify(error));
            res.status(500).json({ "error": "database access error" });
        }
    }

    async updateSnippet(req, res) {
        let result = null;
        try {
            let body = req.body;
            result = await db.dbUpdateSnippet(req.params.id, body.name, body.description, body.author, body.lang, body.code);
            if (result == undefined || result.rows.length == 0)  {
                res.json({ "error": "snippet could not be found" });
            }
            await db.dbDeleteTagsOfSnippet(req.params.id);
            for (let i = 0; i < body.tags.length; i++) {
                let tag = body.tags[i];
                await db.dbCreateTag(tag, result.params.id);
            }
            body.id = result.rows[0];
            res.json(body);
        } catch (error) {
            console.log(JSON.stringify(error));
            res.status(500).json({ "error": "database access error" });
        }
    }

    async querySnippet(req, res) {
        let result = null;
        try {
            let body = req.body;
            let query = req.query;
            let snippetIds = [];
            console.log("QUERY = " + query);
            if (query.name == undefined) query.name = "";
            if (query.description == undefined) query.description = "";
            if (query.author == undefined) query.author = "";
            if (query.lang == undefined) query.lang = "";
            if (query.code == undefined) query.code = "";

            result = await db.dbQuerySnippet(query.name, query.description, query.author, query.lang, query.code);
            result.rows.forEach(row => {
                snippetIds.push(row.id);
            });
            let tagResult = null;
            if (query.tag != undefined) {
                tagResult = await db.dbQueryTags(query.tag);
                tagResult.rows.forEach(row => {
                    snippetIds.push(row.id);
                })
            }
            
            let final = [];
            for (let i = 0; i < snippetIds.length; i++) {
                let id = snippetIds[i];
                let resultSnippet = await db.dbGetSnippet(id);
                let resultTags = await db.dbGetTagsOfSnippet(id);
                if (resultSnippet.rows.length > 0) {
                    let snippet = resultSnippet.rows[0];
                    let tags = [];
                    resultTags.rows.forEach(row => {
                        tags.push(row.name);
                    });
                    snippet.tags = tags;
                    final.push(snippet);
                }
            }
            res.json(final);
        } catch (error) {
            console.log(JSON.stringify(error));
            res.status(500).json({ "error": "database access error" });
        }
    }

    /** Create an Example ReST API 
     * @param {number} port - port number to listen
     * @param {string} prefix - resource path prefix
     * @param {Object} db - database connection
    */
    constructor(port, prefix, db) {
        this.port = port;
        this.prefix = prefix;
        this.db = db;

        // Add Express for routing
        const express = require('express');
        const bodyParser = require('body-parser');

        // Define express app
        this.app = express();
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());

        // Select message by id
        this.app.get(this.prefix + '/', this.querySnippet);
        this.app.post(this.prefix + '/', this.createSnippet);
        this.app.get(this.prefix + '/:id', this.getById);
        this.app.delete(this.prefix + '/:id', this.deleteById);
        this.app.put(this.prefix + '/:id', this.updateSnippet);

        // Listen on given port for requests
        this.server = this.app.listen(this.port, () => {
            var host = this.server.address().address;
            var port = this.server.address().port;
            console.log("SnippetsAPI listening at http://%s:%s%s", host, port, this.prefix);
        });

    }
};

// create database connection
var db = new PSQL_DB(DB_URL);

// create ReST Example API
const api = new SnippetsAPI(SERVER_PORT, SERVER_PREFIX, db);
